package ml.madamita.metani.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by misbahulard on 12/4/2017.
 */

public class JenisTanah {
    @SerializedName("id_jenistanah")
    private int idJenistanah;
    @SerializedName("nama_jenistanah")
    private String namaJenistanah;

    public int getIdJenistanah() {
        return idJenistanah;
    }

    public void setIdJenistanah(int idJenistanah) {
        this.idJenistanah = idJenistanah;
    }

    public String getNamaJenistanah() {
        return namaJenistanah;
    }

    public void setNamaJenistanah(String namaJenistanah) {
        this.namaJenistanah = namaJenistanah;
    }
}
