package ml.madamita.metani.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by misbahulard on 12/4/2017.
 */

public class HasilTani {
    @SerializedName("id_hasil")
    private int idHasil;
    @SerializedName("jenisTani")
    private JenisTani jenisTani;
    @SerializedName("lokasi")
    private Lokasi lokasi;
    @SerializedName("jumlah")
    private int jumlah;

    public int getIdHasil() {
        return idHasil;
    }

    public void setIdHasil(int idHasil) {
        this.idHasil = idHasil;
    }

    public JenisTani getJenisTani() {
        return jenisTani;
    }

    public void setJenisTani(JenisTani jenisTani) {
        this.jenisTani = jenisTani;
    }

    public Lokasi getLokasi() {
        return lokasi;
    }

    public void setLokasi(Lokasi lokasi) {
        this.lokasi = lokasi;
    }

    public int getJumlah() {
        return jumlah;
    }

    public void setJumlah(int jumlah) {
        this.jumlah = jumlah;
    }
}
