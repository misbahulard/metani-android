package ml.madamita.metani.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by misbahulard on 12/4/2017.
 */

public class Tanah {
    @SerializedName("id_tanah")
    private int idTanah;
    @SerializedName("lokasi")
    private Lokasi lokasi;
    @SerializedName("jenisTanah")
    private JenisTanah jenisTanah;
    @SerializedName("luas_tanah")
    private int luasTanah;
    @SerializedName("suhu")
    private int suhu;

    public int getIdTanah() {
        return idTanah;
    }

    public void setIdTanah(int idTanah) {
        this.idTanah = idTanah;
    }

    public Lokasi getLokasi() {
        return lokasi;
    }

    public void setLokasi(Lokasi lokasi) {
        this.lokasi = lokasi;
    }

    public JenisTanah getJenisTanah() {
        return jenisTanah;
    }

    public void setJenisTanah(JenisTanah jenisTanah) {
        this.jenisTanah = jenisTanah;
    }

    public int getLuasTanah() {
        return luasTanah;
    }

    public void setLuasTanah(int luasTanah) {
        this.luasTanah = luasTanah;
    }

    public int getSuhu() {
        return suhu;
    }

    public void setSuhu(int suhu) {
        this.suhu = suhu;
    }
}
