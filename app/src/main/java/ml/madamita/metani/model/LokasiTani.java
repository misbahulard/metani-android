package ml.madamita.metani.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by misbahulard on 12/16/2017.
 */

public class LokasiTani {
    @SerializedName("id_lokasi")
    private int idLokasi;
    @SerializedName("kecamatan")
    private String kecamatan;
    @SerializedName("kabupaten")
    private String kabupaten;
    @SerializedName("provinsi")
    private String provinsi;
    @SerializedName("kode_pos")
    private int kodePos;
    @SerializedName("latitude")
    private double latitude;
    @SerializedName("longtitude")
    private double longtitude;
    @SerializedName("hasilTanis")
    private ArrayList<HasilTani> hasilTanis;

    public int getIdLokasi() {
        return idLokasi;
    }

    public void setIdLokasi(int idLokasi) {
        this.idLokasi = idLokasi;
    }

    public String getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }

    public String getKabupaten() {
        return kabupaten;
    }

    public void setKabupaten(String kabupaten) {
        this.kabupaten = kabupaten;
    }

    public String getProvinsi() {
        return provinsi;
    }

    public void setProvinsi(String provinsi) {
        this.provinsi = provinsi;
    }

    public int getKodePos() {
        return kodePos;
    }

    public void setKodePos(int kodePos) {
        this.kodePos = kodePos;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongtitude() {
        return longtitude;
    }

    public void setLongtitude(double longtitude) {
        this.longtitude = longtitude;
    }

    public ArrayList<HasilTani> getHasilTanis() {
        return hasilTanis;
    }

    public void setHasilTanis(ArrayList<HasilTani> hasilTanis) {
        this.hasilTanis = hasilTanis;
    }
}
