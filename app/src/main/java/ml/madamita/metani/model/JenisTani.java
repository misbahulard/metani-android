package ml.madamita.metani.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by misbahulard on 12/4/2017.
 */

public class JenisTani {
    @SerializedName("id_jenistani")
    private int idJenisTani;
    @SerializedName("nama_jenistani")
    private String namaJenistani;

    public int getIdJenisTani() {
        return idJenisTani;
    }

    public void setIdJenisTani(int idJenisTani) {
        this.idJenisTani = idJenisTani;
    }

    public String getNamaJenistani() {
        return namaJenistani;
    }

    public void setNamaJenistani(String namaJenistani) {
        this.namaJenistani = namaJenistani;
    }
}
