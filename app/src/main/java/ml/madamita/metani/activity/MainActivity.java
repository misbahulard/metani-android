package ml.madamita.metani.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.arlib.floatingsearchview.FloatingSearchView;

import ml.madamita.metani.R;
import ml.madamita.metani.fragment.AboutFragment;
import ml.madamita.metani.fragment.HasilTaniDetailFragment;
import ml.madamita.metani.fragment.HasilTaniFragment;
import ml.madamita.metani.fragment.MapFragment;

public class MainActivity extends AppCompatActivity implements
        NavigationView.OnNavigationItemSelectedListener,
        HasilTaniFragment.OnHasilTaniSelectedListener {

    private FloatingSearchView mFloatingSearchView;
    private DrawerLayout mDrawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Drawer
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        /**
         * Inisialisasi fragment awal {@link HomeFragment}
         */
        if (savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.root_layout, MapFragment.newInstance(), "home")
                    .commit();
        }


        /**
         * Listener untuk hasil yang dipilih oleh user melalui fitur search
         */
        Intent intent = getIntent();

        if (intent.hasExtra("LOKASI_ID")) {
            Bundle bundle = intent.getExtras();
            int id = bundle.getInt("LOKASI_ID");
            double lat = bundle.getDouble("LOKASI_LAT");
            double lng = bundle.getDouble("LOKASI_LONG");

            MapFragment fragment = new MapFragment();
            Bundle args = new Bundle();
            args.putInt("LOKASI_ID", id);
            args.putDouble("LOKASI_LAT", lat);
            args.putDouble("LOKASI_LONG", lng);
            fragment.setArguments(args);

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.root_layout, fragment, "map");
            transaction.commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_map, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                onSearchRequested();
                return true;
            default:
                return false;
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (id) {
            case R.id.nav_map:
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.root_layout, MapFragment.newInstance(), "map")
                        .commit();
                break;
            case R.id.nav_lokasi:
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.root_layout, HasilTaniFragment.newInstance(), "lokasi")
                        .commit();
                break;
            case R.id.nav_about:
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.root_layout, AboutFragment.newInstance(), "about")
                        .commit();
                break;
            default:
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onHasilTaniSelected(int id) {
        HasilTaniDetailFragment fragment = new HasilTaniDetailFragment();
        Bundle args = new Bundle();
        args.putInt(HasilTaniDetailFragment.ID_LOKASI, id);
        fragment.setArguments(args);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.root_layout, fragment, "hasil_tani_detail");
        transaction.addToBackStack("hasil_tani");
        transaction.commit();
    }
}
