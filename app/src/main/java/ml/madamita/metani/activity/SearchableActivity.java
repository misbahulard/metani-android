package ml.madamita.metani.activity;

import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;

import ml.madamita.metani.R;
import ml.madamita.metani.adapter.LokasiAdapter;
import ml.madamita.metani.model.Lokasi;
import ml.madamita.metani.response.LokasiResponse;
import ml.madamita.metani.util.DividerItemDecoration;
import ml.madamita.metani.util.RecyclerTouchListener;
import ml.madamita.metani.util.api.ApiUtils;
import ml.madamita.metani.util.api.BaseApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by misbahulard on 12/23/2017.
 */

public class SearchableActivity extends AppCompatActivity {

    private ArrayList<Lokasi> mLokasis;
    private LokasiAdapter mLokasiAdapter;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private ProgressBar mProgressBar;
    private BaseApiService mBaseApiService;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_searchable);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Search Result");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        mLokasis = new ArrayList<>();
        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_search);
        mLayoutManager = new LinearLayoutManager(this);
        mLokasiAdapter = new LokasiAdapter(this, mLokasis);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayout.VERTICAL));

        mRecyclerView.setAdapter(mLokasiAdapter);

        mRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(this, mRecyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Lokasi lokasi = mLokasis.get(position);
                Intent intent = new Intent(SearchableActivity.this, MainActivity.class);
                intent.putExtra("LOKASI_ID", lokasi.getIdLokasi());
                intent.putExtra("LOKASI_LAT", lokasi.getLatitude());
                intent.putExtra("LOKASI_LONG", lokasi.getLongtitude());
                startActivity(intent);
                finish();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        handleIntent(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            doMySearch(query);
        }
    }

    private void doMySearch(String query) {
        mBaseApiService = ApiUtils.getApiService();

        mBaseApiService.searchLokasi(query).enqueue(new Callback<LokasiResponse>() {
            @Override
            public void onResponse(Call<LokasiResponse> call, Response<LokasiResponse> response) {
                if (response.isSuccessful()) {
                    Boolean success = response.body().getSuccess();
                    if (success) {
                        mLokasis.clear();
                        mLokasis.addAll(response.body().getLokasis());

                        mLokasiAdapter.notifyDataSetChanged();
                        mProgressBar.setVisibility(View.GONE);
                    }
                } else {
                    mProgressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<LokasiResponse> call, Throwable t) {
                mProgressBar.setVisibility(View.GONE);
                Toast.makeText(SearchableActivity.this, "Lokasi tidak ditemukan", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_map, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                onSearchRequested();
                return true;
            default:
                return false;
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
