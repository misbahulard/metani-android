package ml.madamita.metani.fragment;

import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import ml.madamita.metani.R;
import ml.madamita.metani.model.HasilTani;
import ml.madamita.metani.model.Lokasi;
import ml.madamita.metani.model.Tanah;
import ml.madamita.metani.response.LokasiResponse;
import ml.madamita.metani.util.api.ApiUtils;
import ml.madamita.metani.util.api.BaseApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by misbahulard on 12/11/2017.
 */

@SuppressLint("ParcelCreator")
public class MapFragment extends Fragment {

    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;

    private MapView mapView;
    private GoogleMap mMap;

    private ArrayList<Lokasi> mLokasis;
    private ArrayList<HasilTani> mHasilTaniList;
    private ArrayList<Tanah> mTanahList;
    private BaseApiService mBaseApiService;

    public MapFragment() {
    }

    public static MapFragment newInstance() {
        return new MapFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mLokasis = new ArrayList<>();
        mHasilTaniList = new ArrayList<>();
        mTanahList = new ArrayList<>();
        mBaseApiService = ApiUtils.getApiService();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_map, container, false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().show();
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Map");

        // Gets the MapView from the XML layout and creates it
        mapView = (MapView) v.findViewById(R.id.mapview);
        mapView.onCreate(savedInstanceState);
        mapView.onResume();

        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mMap = googleMap;
                mMap.getUiSettings().setCompassEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(true);

                // TODO: Before enabling the My Location layer, you must request
                // location permission from the user. This sample does not include
                // a request for location permission.
                getLocationPermission(mMap);

                /* Check Intent from search */
                Bundle bundle = getArguments();
                if (bundle != null) {
                    int lokasiId = bundle.getInt("LOKASI_ID");
                    double lat = bundle.getDouble("LOKASI_LAT");
                    double lng = bundle.getDouble("LOKASI_LONG");

                    Log.d("MAPFRAG > ", lat + " " + lng);

                    LatLng searchLoc = new LatLng(lat, lng);
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(searchLoc, 13));
                    mMap.addCircle(new CircleOptions()
                            .strokeColor(Color.WHITE)
                            .fillColor(0x554DBBFF)
                            .radius(2000)
                            .center(searchLoc)
                    );
                } else {
                    // move the camera to sidoarjo
                    LatLng sidoarjo = new LatLng(-7.4471541, 112.6718017);
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sidoarjo, 11));
                }

                mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                    @Override
                    public void onInfoWindowClick(Marker marker) {
                        Lokasi lokasi = (Lokasi) marker.getTag();
                        HasilTaniDetailFragment fragment = new HasilTaniDetailFragment();
                        Bundle args = new Bundle();
                        args.putInt(HasilTaniDetailFragment.ID_LOKASI, lokasi.getIdLokasi());
                        fragment.setArguments(args);

                        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.root_layout, fragment, "hasil_tani_detail");
                        transaction.addToBackStack("map");
                        transaction.commit();
                    }
                });

                // Request the data

                mBaseApiService.getAllLokasi().enqueue(new Callback<LokasiResponse>() {
                    @Override
                    public void onResponse(Call<LokasiResponse> call, Response<LokasiResponse> response) {
                        if (response.isSuccessful()) {
                            Boolean success = response.body().getSuccess();
                            if (success) {
                                mLokasis.clear();
                                mLokasis.addAll(response.body().getLokasis());

                                Marker marker;

                                for (int i = 0; i < mLokasis.size(); i++) {
                                    Lokasi lokasi = mLokasis.get(i);
                                    LatLng pos =
                                            new LatLng(lokasi.getLatitude(),
                                                    lokasi.getLongtitude());

                                    String content = lokasi.getKecamatan() + ", "
                                            + lokasi.getKabupaten() + ", "
                                            + lokasi.getProvinsi();

                                    marker = mMap.addMarker(new MarkerOptions()
                                            .position(pos)
                                            .title("Hasil Tani")
                                            .snippet(content)
                                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));

                                    marker.setTag(lokasi);

                                }
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<LokasiResponse> call, Throwable t) {

                    }
                });

            }
        });

        return v;
    }

    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    private void getLocationPermission(GoogleMap map) {
    /*
     * Request location permission, so that we can get the location of the
     * device. The result of the permission request is handled by a callback,
     * onRequestPermissionsResult.
     */
        if (ContextCompat.checkSelfPermission(getActivity().getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            map.setMyLocationEnabled(true);

        } else {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }
}
