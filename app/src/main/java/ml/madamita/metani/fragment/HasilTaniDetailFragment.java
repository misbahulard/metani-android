package ml.madamita.metani.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import ml.madamita.metani.R;
import ml.madamita.metani.adapter.TanahAdapter;
import ml.madamita.metani.adapter.TaniAdapter;
import ml.madamita.metani.model.HasilTani;
import ml.madamita.metani.model.LokasiTanah;
import ml.madamita.metani.model.LokasiTani;
import ml.madamita.metani.model.Tanah;
import ml.madamita.metani.response.LokasiTanahResponse;
import ml.madamita.metani.response.LokasiTaniResponse;
import ml.madamita.metani.util.api.ApiUtils;
import ml.madamita.metani.util.api.BaseApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by misbahulard on 12/16/2017.
 */

public class HasilTaniDetailFragment extends Fragment {
    public static final String ID_LOKASI = "BUNDLE_ID_LOKASI";
    private LokasiTani mLokasiTani;
    private LokasiTanah mLokasiTanah;
    private ArrayList<HasilTani> mHasilTanis;
    private ArrayList<Tanah> mTanahs;

    private TaniAdapter mTaniAdapter;
    private TanahAdapter mTanahAdapter;
    private RecyclerView mRecyclerViewTani, mRecyclerViewTanah;
    private LinearLayoutManager mLayoutManagerTani, mLayoutManagerTanah;
    private BaseApiService mBaseApiService;
    private ProgressBar mProgressBarInit;

    private ConstraintLayout mLayoutHasilTaniContainer;
    private TextView mTvKecamatan, mTvAddress;

    public HasilTaniDetailFragment() {
    }

    public static HasilTaniDetailFragment newInstance() {
        return new HasilTaniDetailFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLokasiTani = new LokasiTani();
        mLokasiTanah = new LokasiTanah();
        mHasilTanis = new ArrayList<HasilTani>();
        mTanahs = new ArrayList<Tanah>();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_hasil_tani_detail, container, false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().show();
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Detail Lokasi");

        /**
         * Init View
         */
        mLayoutHasilTaniContainer = (ConstraintLayout) view.findViewById(R.id.constraint_hasil_tani_container);
        mTvKecamatan = (TextView) view.findViewById(R.id.text_kecamatan);
        mTvAddress = (TextView) view.findViewById(R.id.text_alamat);

        /**
         * Inisialisasi RecyclerView berserta adapter
         */
        mProgressBarInit = (ProgressBar) view.findViewById(R.id.progress_bar_init);
        mRecyclerViewTani = (RecyclerView) view.findViewById(R.id.recycler_hasil_tani);
        mRecyclerViewTanah = (RecyclerView) view.findViewById(R.id.recycler_tanah);
        mLayoutManagerTani = new LinearLayoutManager(getActivity());
        mLayoutManagerTanah = new LinearLayoutManager(getActivity());
        mRecyclerViewTani.setLayoutManager(mLayoutManagerTani);
        mRecyclerViewTanah.setLayoutManager(mLayoutManagerTanah);
        mRecyclerViewTani.setItemAnimator(new DefaultItemAnimator());
        mRecyclerViewTanah.setItemAnimator(new DefaultItemAnimator());

        mTaniAdapter = new TaniAdapter(getActivity(), mHasilTanis);
        mTanahAdapter = new TanahAdapter(getActivity(), mTanahs);
        mRecyclerViewTani.setAdapter(mTaniAdapter);
        mRecyclerViewTanah.setAdapter(mTanahAdapter);

        getData();

        return view;
    }

    private void getData() {

        mProgressBarInit.setVisibility(View.VISIBLE);
        /**
         * Mengambil Bundle yang dikirim oleh activity / fragment lain
         * Bundle ini berisi data ID dari suatu lokasi
         */
        Bundle bundle = getArguments();
        int idLokasi = bundle.getInt(ID_LOKASI);

        mBaseApiService = ApiUtils.getApiService();

        /**
         * LOKASI TANI
         */
        mBaseApiService.getAllLokasiTani(idLokasi).enqueue(new Callback<LokasiTaniResponse>() {
            @Override
            public void onResponse(Call<LokasiTaniResponse> call, Response<LokasiTaniResponse> response) {
                if (response.isSuccessful()) {
                    Boolean success = response.body().getSuccess();
                    if (success) {
                        mLokasiTani = response.body().getLokasiTanis().get(0);
                        mHasilTanis.clear();
                        mHasilTanis.addAll(mLokasiTani.getHasilTanis());

                        /**
                         * SET VIEW
                         */
                        String kecamatan = mLokasiTani.getKecamatan();
                        String alamat = mLokasiTani.getKabupaten() + ", " + mLokasiTani.getProvinsi();
                        mTvKecamatan.setText(kecamatan);
                        mTvAddress.setText(alamat);

                        mLayoutHasilTaniContainer.setVisibility(View.VISIBLE);

                        mTaniAdapter.notifyDataSetChanged();
                        mProgressBarInit.setVisibility(View.GONE);
                    }
                } else {
                    String error_message = "Can't get data";
                    Toast.makeText(getContext(), error_message, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<LokasiTaniResponse> call, Throwable t) {
                Toast.makeText(getActivity(), "Please refresh", Toast.LENGTH_LONG).show();
            }
        });

        /**
         * LOKASI TANAH
         */
        mBaseApiService.getAllLokasiTanah(idLokasi).enqueue(new Callback<LokasiTanahResponse>() {
            @Override
            public void onResponse(Call<LokasiTanahResponse> call, Response<LokasiTanahResponse> response) {
                if (response.isSuccessful()) {
                    Boolean success = response.body().getSuccess();
                    if (success) {
                        mLokasiTanah = response.body().getLokasiTanahs().get(0);
                        mTanahs.clear();
                        mTanahs.addAll(mLokasiTanah.getTanahs());

                        mTanahAdapter.notifyDataSetChanged();
                        mProgressBarInit.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<LokasiTanahResponse> call, Throwable t) {

            }
        });
    }
}
