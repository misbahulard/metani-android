package ml.madamita.metani.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.ArrayList;

import ml.madamita.metani.R;
import ml.madamita.metani.adapter.LokasiAdapter;
import ml.madamita.metani.model.Lokasi;
import ml.madamita.metani.response.LokasiResponse;
import ml.madamita.metani.util.DividerItemDecoration;
import ml.madamita.metani.util.RecyclerTouchListener;
import ml.madamita.metani.util.api.ApiUtils;
import ml.madamita.metani.util.api.BaseApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by misbahulard on 12/16/2017.
 */

public class HasilTaniFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    private final int PAGE_SIZE = 20;
    private int mPageIndex = 1;

    private boolean isRefreshing = false;
    private boolean isLoading = false;
    private boolean isLastPage = false;

    private ProgressBar mProgressBarNext, mProgressBarInit;

    private ArrayList<Lokasi> mLokasis;
    private LokasiAdapter mLokasiAdapter;
    private BaseApiService mBaseApiService;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    private OnHasilTaniSelectedListener mHasilTaniSelectedListener;

    public HasilTaniFragment() {
    }

    public static HasilTaniFragment newInstance() {
        return new HasilTaniFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            mHasilTaniSelectedListener = (HasilTaniFragment.OnHasilTaniSelectedListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnHasilTaniSelectedListener");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLokasis = new ArrayList<>();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_hasil_tani, container, false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().show();
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Hasil Tani");

        /**
         * Inisialisasi RecyclerView berserta adapter
         */
        mProgressBarInit = (ProgressBar) view.findViewById(R.id.progress_bar_init);
        mProgressBarNext = (ProgressBar) view.findViewById(R.id.progress_bar_next);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_hasil_tani);
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));

        mLokasiAdapter = new LokasiAdapter(getActivity(), mLokasis);
        mRecyclerView.setAdapter(mLokasiAdapter);

        // Get Data from API
        getData();

        mRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(getContext(), mRecyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Lokasi lokasi = mLokasis.get(position);
                mHasilTaniSelectedListener.onHasilTaniSelected(lokasi.getIdLokasi());
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = mLayoutManager.getChildCount();
                int totalItemCount = mLayoutManager.getItemCount();
                int firstVisibleItemPosition = mLayoutManager.findFirstVisibleItemPosition();

                if (!isLoading && !isLastPage) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0
                            && totalItemCount >= PAGE_SIZE) {

                        // Set progressbar visible
                        isLoading = true;
                        mProgressBarNext.setVisibility(View.VISIBLE);
                        getNextData();
                    }
                }
            }
        });

        mSwipeRefreshLayout.post(
                new Runnable() {
                    @Override
                    public void run() {
                        getData();
                    }
                }
        );

        return view;
    }

    private void getData() {
        if (!isRefreshing) {
            mProgressBarInit.setVisibility(View.VISIBLE);
        } else {
            isRefreshing = false;
        }
        mBaseApiService = ApiUtils.getApiService();

        mBaseApiService.getAllLokasi().enqueue(new Callback<LokasiResponse>() {
            @Override
            public void onResponse(Call<LokasiResponse> call, Response<LokasiResponse> response) {
                if (response.isSuccessful()) {
                    Boolean success = response.body().getSuccess();
                    if (success) {
                        mLokasis.clear();
                        mLokasis.addAll(response.body().getLokasis());

                        mLokasiAdapter.notifyDataSetChanged();
                        mSwipeRefreshLayout.setRefreshing(false);
                        mProgressBarInit.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<LokasiResponse> call, Throwable t) {

            }
        });

    }

    private void getNextData() {
        mBaseApiService = ApiUtils.getApiService();

        mBaseApiService.getAllLokasiPagination(mPageIndex).enqueue(new Callback<LokasiResponse>() {
            @Override
            public void onResponse(Call<LokasiResponse> call, Response<LokasiResponse> response) {
                if (response.isSuccessful()) {
                    Boolean success = response.body().getSuccess();
                    if (success) {

                        ArrayList<Lokasi> lokasis = response.body().getLokasis();
                        if (lokasis != null) {
                            mLokasis.addAll(lokasis);

                            mLokasiAdapter.notifyDataSetChanged();
                            mProgressBarNext.setVisibility(View.GONE);
                            // update page
                            mPageIndex++;
                            isLoading = false;
                        } else {
                            isLastPage = true;
                        }


                    }
                }
            }

            @Override
            public void onFailure(Call<LokasiResponse> call, Throwable t) {

            }
        });

    }

    @Override
    public void onRefresh() {
        isRefreshing = true;
        getData();
    }

    /**
     * Event listener interface
     */
    public interface OnHasilTaniSelectedListener {
        void onHasilTaniSelected(int id);
    }
}
