package ml.madamita.metani.util.api;

import ml.madamita.metani.response.HasilTaniResponse;
import ml.madamita.metani.response.LokasiResponse;
import ml.madamita.metani.response.LokasiTanahResponse;
import ml.madamita.metani.response.LokasiTaniResponse;
import ml.madamita.metani.response.TanahResponse;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface BaseApiService {
    @GET("lokasi")
    Call<LokasiResponse> getAllLokasi();

    @GET("lokasi")
    Call<LokasiResponse> getAllLokasiPagination(@Query("page") int pageIndex);

    @GET("lokasi/lokasi-tani/{id}")
    Call<LokasiTaniResponse> getAllLokasiTani(@Path("id") int id);

    @GET("lokasi/lokasi-tanah/{id}")
    Call<LokasiTanahResponse> getAllLokasiTanah(@Path("id") int id);

    @POST("lokasi/search")
    @FormUrlEncoded
    Call<LokasiResponse> searchLokasi(@Field("q") String query);

    @GET("hasil-tani")
    Call<HasilTaniResponse> getAllHasilTani();

    @GET("hasil-tani")
    Call<HasilTaniResponse> getAllHasilTaniPagination(@Query("page") int pageIndex);

    @GET("tanah")
    Call<TanahResponse> getAllTanah();

    @GET("tanah")
    Call<TanahResponse> getAllTanahPagination(@Query("page") int pageIndex);
}
