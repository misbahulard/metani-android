package ml.madamita.metani.util;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPrefManager {
    public static final String PREF_APP = "prefMetaniApp";

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor spEditor;

    /**
     * Constructor untuk membuat Shared Preferences baru
     *
     * @param context activity shared preferences dibuat
     */
    public SharedPrefManager(Context context) {
        sharedPreferences = context.getSharedPreferences(PREF_APP, Context.MODE_PRIVATE);
        spEditor = sharedPreferences.edit();
    }

    /**
     * Method yang berfungsi untuk menyimpan preferences bertipe data string
     *
     * @param key   kata kunci shared preferences yang akan disimpan nilainya
     * @param value (String) nilai yang akan disimpan ke dalam kunci shared preferences
     */
    public void savePrefString(String key, String value) {
        spEditor.putString(key, value);
        spEditor.commit();
    }

    /**
     * Method yang berfungsi untuk menyimpan preferences bertipe data integer
     *
     * @param key   kata kunci shared preferences yang akan disimpan nilainya
     * @param value (int) nilai yang akan disimpan ke dalam kunci shared preferences
     */
    public void savePrefInt(String key, int value) {
        spEditor.putInt(key, value);
        spEditor.commit();
    }

    /**
     * Method yang berfungsi untuk menyimpan preferences bertipe data boolean
     *
     * @param key   kata kunci shared preferences yang akan disimpan nilainya
     * @param value (boolean) nilai yang akan disimpan ke dalam kunci shared preferences
     */
    public void savePrefBoolean(String key, boolean value) {
        spEditor.putBoolean(key, value);
        spEditor.commit();
    }

    /**
     * Method yang berfungsi untuk menghapus semua shared preferences
     */
    public void clearPref() {
        spEditor.clear().commit();
    }

}
