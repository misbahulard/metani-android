package ml.madamita.metani.util;

import android.app.SearchManager;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;

import ml.madamita.metani.model.Lokasi;
import ml.madamita.metani.response.LokasiResponse;
import ml.madamita.metani.util.api.ApiUtils;
import ml.madamita.metani.util.api.BaseApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by misbahulard on 12/23/2017.
 */

public class LokasiSearchContentProvider extends ContentProvider {

    private static final String STORES = "stores/" + SearchManager.SUGGEST_URI_PATH_QUERY + "/*";

    private static final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    private static String[] matrixCursorColumns = {
            "_id",
            SearchManager.SUGGEST_COLUMN_TEXT_1,
            SearchManager.SUGGEST_COLUMN_INTENT_DATA
    };

    static {
        uriMatcher.addURI("ml.madamita.metani.lokasi.search", STORES, 1);
    }

    private BaseApiService mBaseApiService;

    @Override
    public boolean onCreate() {
        mBaseApiService = ApiUtils.getApiService();
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {

        String queryType = "";
        switch (uriMatcher.match(uri)) {
            case 1:
                String query = uri.getLastPathSegment().toLowerCase();
                return getSearchResultsCursor(query);
            default:
                return null;
        }
    }

    private MatrixCursor getSearchResultsCursor(String searchString) {
        final MatrixCursor searchResults = new MatrixCursor(matrixCursorColumns);
        if (searchString != null) {
            searchString = searchString.toLowerCase();

            // Call retrofit here
            final String finalSearchString = searchString;
            mBaseApiService.searchLokasi(searchString).enqueue(new Callback<LokasiResponse>() {
                @Override
                public void onResponse(Call<LokasiResponse> call, Response<LokasiResponse> response) {
                    if (response.isSuccessful()) {

                        ArrayList<Lokasi> lokasis = response.body().getLokasis();
                        Object[] mRow = new Object[3];
                        int counterId = 0;

                        for (Lokasi lokasi : lokasis) {
                            if (lokasi.getKecamatan().toLowerCase().contains(finalSearchString)) {
                                mRow[0] = "" + counterId++;
                                mRow[1] = lokasi.getKecamatan();
                                mRow[2] = String.valueOf(lokasi.getIdLokasi());

                                searchResults.addRow(mRow);
                            }
                        }

                    }
                }

                @Override
                public void onFailure(Call<LokasiResponse> call, Throwable t) {
                    Log.d("SEARCH", "Failure to search");
                }
            });
        }
        return searchResults;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues contentValues) {
        return null;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String s, @Nullable String[] strings) {
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues contentValues, @Nullable String s, @Nullable String[] strings) {
        return 0;
    }
}
