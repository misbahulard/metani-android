package ml.madamita.metani.util.api;

public class ApiUtils {
    public static final String BASE_URL_API = "http://api.metani.madamita.ml/";
    public static final String BASE_URL = "http://metani.madamita.ml/";

    /**
     * Method ini berfungsi untuk mendapatkan Api Service
     *
     * @return BaseApiService dari OkHttp
     */
    public static BaseApiService getApiService() {
        return RetrofitClient.getClient(BASE_URL_API).create(BaseApiService.class);
    }
}
