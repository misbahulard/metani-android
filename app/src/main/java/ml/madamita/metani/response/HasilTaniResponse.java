package ml.madamita.metani.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import ml.madamita.metani.model.HasilTani;
import ml.madamita.metani.model.Meta;

/**
 * Created by misbahulard on 12/5/2017.
 */

public class HasilTaniResponse {
    @SerializedName("success")
    private Boolean success;
    @SerializedName("data")
    private ArrayList<HasilTani> hasilTaniList;
    @SerializedName("meta")
    private Meta meta;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public ArrayList<HasilTani> getHasilTaniList() {
        return hasilTaniList;
    }

    public void setHasilTaniList(ArrayList<HasilTani> hasilTaniList) {
        this.hasilTaniList = hasilTaniList;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }
}
