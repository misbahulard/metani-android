package ml.madamita.metani.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import ml.madamita.metani.model.Lokasi;
import ml.madamita.metani.model.Meta;

/**
 * Created by misbahulard on 12/16/2017.
 */


public class LokasiResponse {
    @SerializedName("success")
    private Boolean success;
    @SerializedName("data")
    private ArrayList<Lokasi> lokasis;
    @SerializedName("meta")
    private Meta meta;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public ArrayList<Lokasi> getLokasis() {
        return lokasis;
    }

    public void setLokasis(ArrayList<Lokasi> lokasis) {
        this.lokasis = lokasis;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }
}
