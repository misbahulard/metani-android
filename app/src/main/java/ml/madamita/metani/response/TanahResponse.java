package ml.madamita.metani.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import ml.madamita.metani.model.Meta;
import ml.madamita.metani.model.Tanah;

/**
 * Created by misbahulard on 12/5/2017.
 */

public class TanahResponse {
    @SerializedName("success")
    private Boolean success;
    @SerializedName("data")
    private ArrayList<Tanah> tanahList;
    @SerializedName("meta")
    private Meta meta;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public ArrayList<Tanah> getTanahList() {
        return tanahList;
    }

    public void setTanahList(ArrayList<Tanah> tanahList) {
        this.tanahList = tanahList;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }
}
