package ml.madamita.metani.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import ml.madamita.metani.model.LokasiTani;
import ml.madamita.metani.model.Meta;

/**
 * Created by misbahulard on 12/16/2017.
 */

public class LokasiTaniResponse {
    @SerializedName("success")
    private Boolean success;
    @SerializedName("data")
    private ArrayList<LokasiTani> lokasiTanis;
    @SerializedName("meta")
    private Meta meta;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public ArrayList<LokasiTani> getLokasiTanis() {
        return lokasiTanis;
    }

    public void setLokasiTanis(ArrayList<LokasiTani> lokasiTanis) {
        this.lokasiTanis = lokasiTanis;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }
}
