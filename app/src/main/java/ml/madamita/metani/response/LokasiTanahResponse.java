package ml.madamita.metani.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import ml.madamita.metani.model.LokasiTanah;
import ml.madamita.metani.model.Meta;

/**
 * Created by misbahulard on 12/16/2017.
 */

public class LokasiTanahResponse {
    @SerializedName("success")
    private Boolean success;
    @SerializedName("data")
    private ArrayList<LokasiTanah> lokasiTanahs;
    @SerializedName("meta")
    private Meta meta;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public ArrayList<LokasiTanah> getLokasiTanahs() {
        return lokasiTanahs;
    }

    public void setLokasiTanahs(ArrayList<LokasiTanah> lokasiTanahs) {
        this.lokasiTanahs = lokasiTanahs;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }
}
