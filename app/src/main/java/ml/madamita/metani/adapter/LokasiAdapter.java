package ml.madamita.metani.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import ml.madamita.metani.R;
import ml.madamita.metani.model.Lokasi;

/**
 * Created by misbahulard on 12/16/2017.
 */

public class LokasiAdapter extends RecyclerView.Adapter<LokasiAdapter.LokasiAdapterViewHolder> {
    private Context mContext;
    private ArrayList<Lokasi> mLokasi;

    public LokasiAdapter(Context mContext, ArrayList<Lokasi> lokasis) {
        this.mContext = mContext;
        this.mLokasi = lokasis;
    }

    @Override
    public LokasiAdapter.LokasiAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_hasil_tani, parent, false);
        return new LokasiAdapter.LokasiAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(LokasiAdapter.LokasiAdapterViewHolder holder, int position) {
        Lokasi lokasi = mLokasi.get(position);
        String kecamatan = lokasi.getKecamatan();
        String alamat = lokasi.getKabupaten() + ", " + lokasi.getProvinsi();
        holder.mTvKecamatan.setText(kecamatan);
        holder.mTvAlamat.setText(alamat);
    }

    @Override
    public int getItemCount() {
        return mLokasi.size();
    }

    class LokasiAdapterViewHolder extends RecyclerView.ViewHolder {
        private TextView mTvKecamatan, mTvAlamat;

        public LokasiAdapterViewHolder(View itemView) {
            super(itemView);
            mTvKecamatan = (TextView) itemView.findViewById(R.id.text_kecamatan);
            mTvAlamat = (TextView) itemView.findViewById(R.id.text_lokasi);
        }
    }
}
