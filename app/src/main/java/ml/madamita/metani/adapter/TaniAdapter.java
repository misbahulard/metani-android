package ml.madamita.metani.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import ml.madamita.metani.R;
import ml.madamita.metani.model.HasilTani;

/**
 * Created by misbahulard on 12/12/2017.
 */

public class TaniAdapter extends RecyclerView.Adapter<TaniAdapter.TaniAdapterViewHolder> {

    private Context mContext;
    private ArrayList<HasilTani> mHasilTanis;

    public TaniAdapter(Context mContext, ArrayList<HasilTani> hasilTanis) {
        this.mContext = mContext;
        this.mHasilTanis = hasilTanis;
    }

    @Override
    public TaniAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_tani, parent, false);
        return new TaniAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TaniAdapterViewHolder holder, int position) {
        HasilTani hasilTani = mHasilTanis.get(position);
        String nama = hasilTani.getJenisTani().getNamaJenistani();
        String jumlah = hasilTani.getJumlah() + " Ton";
        holder.mTvNama.setText(nama);
        holder.mTvJumlah.setText(jumlah);
    }

    @Override
    public int getItemCount() {
        return mHasilTanis.size();
    }

    class TaniAdapterViewHolder extends RecyclerView.ViewHolder {
        private TextView mTvNama, mTvJumlah;

        public TaniAdapterViewHolder(View itemView) {
            super(itemView);
            mTvNama = (TextView) itemView.findViewById(R.id.text_nama_tani);
            mTvJumlah = (TextView) itemView.findViewById(R.id.text_jumlah_tani);
        }
    }
}
