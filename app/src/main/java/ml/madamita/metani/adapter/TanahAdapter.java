package ml.madamita.metani.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import ml.madamita.metani.R;
import ml.madamita.metani.model.Tanah;

/**
 * Created by misbahulard on 12/14/2017.
 */

public class TanahAdapter extends RecyclerView.Adapter<TanahAdapter.TanahAdapterViewHolder>{
    final String DEGREE  = "\u00b0";
    private Context mContext;
    private ArrayList<Tanah> mTanahs;

    public TanahAdapter(Context mContext, ArrayList<Tanah> hasilTanis) {
        this.mContext = mContext;
        this.mTanahs = hasilTanis;
    }

    @Override
    public TanahAdapter.TanahAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_tanah, parent, false);
        return new TanahAdapter.TanahAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TanahAdapter.TanahAdapterViewHolder holder, int position) {
        Tanah tanah = mTanahs.get(position);
        String nama = tanah.getJenisTanah().getNamaJenistanah();
        String luas = tanah.getLuasTanah() + " Ha";
        String suhu = tanah.getSuhu() + DEGREE + "C";
        holder.mTvNama.setText(nama);
        holder.mTvLuas.setText(luas);
        holder.mTvSuhu.setText(suhu);
    }

    @Override
    public int getItemCount() {
        return mTanahs.size();
    }

    class TanahAdapterViewHolder extends RecyclerView.ViewHolder {
        private TextView mTvNama, mTvLuas, mTvSuhu;

        public TanahAdapterViewHolder(View itemView) {
            super(itemView);
            mTvNama = (TextView) itemView.findViewById(R.id.text_nama_tanah);
            mTvLuas = (TextView) itemView.findViewById(R.id.text_luas_tanah);
            mTvSuhu = (TextView) itemView.findViewById(R.id.text_suhu_tanah);
        }
    }
}
