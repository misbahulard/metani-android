package ml.madamita.metani.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import ml.madamita.metani.model.Lokasi;

/**
 * Created by misbahulard on 12/23/2017.
 */

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.SearchAdapterViewHolder> {
    private ArrayList<Lokasi> mLokasis;

    public SearchAdapter(ArrayList<Lokasi> mLokasis) {
        this.mLokasis = mLokasis;
    }

    @Override
    public SearchAdapter.SearchAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(SearchAdapter.SearchAdapterViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    class SearchAdapterViewHolder extends RecyclerView.ViewHolder {
        private TextView mTvAddress;

        public SearchAdapterViewHolder(View itemView) {
            super(itemView);
//            mTvAddress = (TextView) itemView.findViewById(R.id.text_address);
        }
    }
}
